import { logger } from 'firebase-functions'
import { DocumentSnapshot, FieldValue } from 'firebase-admin/firestore'
import { ethers } from 'ethers'
import { EventUpdateListener, shouldUpdateBlockchainMoment } from './logUtils'
import { BlockchainMoment, BlockchainValue, HexString } from './types'
import { JumpPort, MoonCatAcclimator } from '@mooncatrescue/contracts/moonCatUtils'

export const MOMENTS_COLLECTION = 'moments'
export const MOMENTS_ADDRESS = '0x367721b332F4697d440EBBe6780262411Fd03409'

export function getMomentOwnerFromDoc(momentDoc: DocumentSnapshot): BlockchainValue<HexString> | null {
  if (!momentDoc.exists) return null // Document doesn't actually exist in the Firebase store
  const momentData = momentDoc.data()
  if (typeof momentData == 'undefined') return null // Document exists, but has no data
  if (typeof momentData.owner === 'undefined') return null // Document exists, but has no ownership information

  if (momentData.owner[JumpPort.address] && momentData.owner[JumpPort.address].value !== false) {
    // JumpPort knows of this Moment; Moment is staked there and therefore what JumpPort reports as the owner is the true owner
    return momentData.owner[JumpPort.address]
  } else if (momentData.owner[MOMENTS_ADDRESS] && momentData.owner[MOMENTS_ADDRESS].value !== false) {
    // The core MoonCatMoments contract's ownership is the true owner
    return momentData.owner[MOMENTS_ADDRESS]
  }

  // Corrupted document; none of the MoonCatRescue-related contracts have ownership info for this Moment
  throw new Error('Unknown owner')
}

export const getOwnedMoments = async function ({
  collection,
  dbtx,
  targetAddress,
  ownedMoonCats,
}: {
  collection: FirebaseFirestore.CollectionReference
  dbtx: FirebaseFirestore.Transaction
  targetAddress: HexString
  ownedMoonCats: number[]
}) {
  logger.debug(`Doing database lookup of all Moments for address ${targetAddress}...`)

  let moments: { momentId: number; moonCat?: number }[] = []

  // Find all Moments that are owned by one of the MoonCats owned by this address
  // The Firestore 'in' comparator can only take up to 30 items at a time
  const CHUNK_SIZE = 30
  let ownedByMoonCats: { momentId: number; moonCat?: number }[] = []
  for (let i = 0; i < ownedMoonCats.length; i += CHUNK_SIZE) {
    let rs = await dbtx.get(
      collection
        .where(`owner.${MOMENTS_ADDRESS}.value`, '==', MoonCatAcclimator.address)
        .where(`owner.${MoonCatAcclimator.address}-998.value`, 'in', ownedMoonCats.slice(i, i + CHUNK_SIZE))
    )
    ownedByMoonCats = ownedByMoonCats.concat(
      rs.docs.map((d) => ({
        momentId: Number(d.id),
        moonCat: d.get(`owner.${MoonCatAcclimator.address}-998.value`),
      }))
    )
  }
  logger.debug(`Moments with MoonCats owned by ${targetAddress}: ${ownedByMoonCats.length}`)
  moments = moments.concat(ownedByMoonCats)

  // Find all Moments that are directly-held
  if (targetAddress != MoonCatAcclimator.address) {
    let rs = await dbtx.get(collection.where(`owner.${MOMENTS_ADDRESS}.value`, '==', targetAddress))
    logger.debug(`Moments directly owned by ${targetAddress}: ${rs.size}`)
    moments = moments.concat(
      rs.docs.map((d) => ({
        momentId: Number(d.id),
      }))
    )
  }

  return moments
}

/**
 * Update dependent documents for Transfer Events
 *
 * Called when a Transfer Event from the MoonCatMoments contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMomentTransferred: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { from, to, tokenId } = evtLog.args as { from: HexString; to: HexString; tokenId: number }
  const docRef = db.collection(MOMENTS_COLLECTION).doc(String(tokenId))
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  const logPreamble = `Transfer event seen for Moment ${tokenId}`

  if (from == ethers.constants.AddressZero) {
    // Moment got minted into existence
    let doc: { owner: { [addr: HexString]: { value: any; checked?: BlockchainMoment; modified?: BlockchainMoment } } } =
      { owner: {} }

    doc.owner[MOMENTS_ADDRESS] = { value: to, modified: eventModified }
    dbtx.set(docRef, doc, { merge: true })
    logger.info(`${logPreamble}, minting into existence.`)
  } else {
    // Moment moved from one location to another
    const snapshot = await dbtx.get(docRef)
    if (!snapshot.exists) {
      logger.error(`${logPreamble}, but no meta document exists for that Moment`)
      return
    }
    const moment = await snapshot.data()
    const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
      moment!.owner[MOMENTS_ADDRESS],
      evtLog.blockNumber
    )
    let updateMoment: { [key: string]: any } = {}
    if (shouldClearChecked) updateMoment[`owner.${MOMENTS_ADDRESS}.checked`] = FieldValue.delete()
    if (shouldUpdateValue) updateMoment[`owner.${MOMENTS_ADDRESS}.value`] = to
    if (shouldUpdateModified) updateMoment[`owner.${MOMENTS_ADDRESS}.modified`] = eventModified
    await dbtx.update(docRef, updateMoment)
    if (shouldUpdateValue) {
      logger.info(`${logPreamble}, moving to ${to}. Moment metadata updated.`)
    } else if (shouldUpdateModified) {
      logger.debug(`${logPreamble}, moving to ${to}. Updating last-modified record.`)
    }
  }
}
