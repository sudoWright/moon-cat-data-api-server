import { logger } from 'ethers'
import { Timestamp, getFirestore } from 'firebase-admin/firestore'
import { onDocumentWritten } from 'firebase-functions/v2/firestore'
import { ADDR_META_COLLECTION } from './logUtils'
import { MOMENTS_COLLECTION, getMomentOwnerFromDoc, getOwnedMoments } from './momentUtils'
import { MoonCatSummary } from './moonCatUtils'
import { parseOwnershipUpdates } from './ownerProfiles'

// The Firebase Admin SDK to access Firestore.
const db = getFirestore()

/**
 * Whenever a Moment's metadata is updated, update ownership listing.
 *
 * This is a Firebase hook function that automatically fires whenever a Firestore document matching the criteria is updated.
 */
export const onMomentUpdated = onDocumentWritten(`/${MOMENTS_COLLECTION}/{momentId}`, async (event) => {
  if (typeof event == 'undefined' || typeof event.data == 'undefined') return
  const momentId = event.params.momentId

  const ownersNeedingUpdates = await parseOwnershipUpdates(
    `Moment ${momentId}`,
    event.data.after,
    event.data.before,
    getMomentOwnerFromDoc
  )

  // Loop through all addresses needing updates
  for (let addr in ownersNeedingUpdates) {
    const docRef = db.collection(ADDR_META_COLLECTION).doc(addr)

    // Each individual address gets its own transaction. Because if something modifies the Moment records
    // this address owns, we want Firebase to re-run just that address' job
    await db.runTransaction(async (dbtx) => {
      const doc = await dbtx.get(docRef)
      let newData: { [key: string]: any } = {}
      if (!doc.exists) {
        // First time this document is being created; initialize other parameters
        newData.checked = null
      }

      // Fetch the data needed to update the document
      logger.debug(`Starting refresh of address Moment ownership metadata for ${addr}`)
      const ownedMoonCats: MoonCatSummary[] | undefined = doc.get('ownedMoonCats.list')
      const ownedMoments = await getOwnedMoments({
        collection: db.collection(MOMENTS_COLLECTION),
        dbtx,
        targetAddress: addr as `0x${string}`,
        ownedMoonCats: typeof ownedMoonCats == 'undefined' ? [] : ownedMoonCats.map((m) => m.rescueOrder),
      })
      newData.ownedMoments = {
        list: ownedMoments,
        count: ownedMoments.length,
        modified: Timestamp.now(),
      }
      await dbtx.set(docRef, newData, { merge: true })
    })
  }
})
