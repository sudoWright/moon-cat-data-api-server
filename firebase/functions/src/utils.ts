import { Request } from 'firebase-functions/v2/https'

export function getPathSuffix(req: Request, urlBase: string): string | undefined {
  let pos = req.path.indexOf(urlBase)
  if (pos < 0) {
    return undefined
  }
  return req.path.substring(pos + urlBase.length).trim()
}
