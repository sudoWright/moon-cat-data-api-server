import { DocumentSnapshot, FieldValue } from 'firebase-admin/firestore'
import { logger } from 'firebase-functions'
import { MoonCatRescue, MoonCatAcclimator, OldWrappedMoonCats, JumpPort } from '@mooncatrescue/contracts/moonCatUtils'
import { shouldUpdateBlockchainMoment, EventUpdateListener, EventLog } from './logUtils'
import { HexString, BlockchainMoment, BlockchainValue } from './types'
import { MOMENTS_ADDRESS, MOMENTS_COLLECTION } from './momentUtils'

export const MOONCAT_COLLECTION = 'mooncats'

/**
 * Take a MoonCat name (a raw 32-byte value) and attempt to parse it as a UTF-8 string.
 *
 * Returns the UTF-8 formatted value, or `true` if it cannot be parsed.
 */
export function rawNameToString(rawName: HexString): string | true {
  let nameBuffer = Buffer.from(rawName.slice(2), 'hex')
  let firstNull = nameBuffer.indexOf(0x00)
  if (firstNull >= 0) {
    nameBuffer = nameBuffer.subarray(0, firstNull)
  }
  try {
    let nameString = new TextDecoder('utf8', { fatal: true }).decode(nameBuffer)
    return nameString
  } catch (err) {
    return true
  }
}

/**
 * Identify a MoonCat by input query
 *
 * Given an input value that is either a rescue order or a hex ID value, determine the other identifier if the application has trait data for that MoonCat
 */
export async function parseMoonCatIdentifier(
  moonCatCollection: FirebaseFirestore.CollectionReference,
  input: string
): Promise<{ rescueOrder: number | false; hexId: string | false }> {
  if (isNaN(Number(input))) {
    return {
      rescueOrder: false,
      hexId: false,
    }
  }
  if (input.substring(0, 2) == '0x') {
    // Input is the Hex ID; try to look up the rescue order
    const snapshot = await moonCatCollection.where('catId', '==', input).limit(1).get()

    return {
      hexId: input,
      rescueOrder: snapshot.empty ? false : snapshot.docs[0].get('rescueOrder'),
    }
  } else {
    // Input is the rescue order; try and look up the Hex ID
    const snapshot = await moonCatCollection.where('rescueOrder', '==', parseInt(input)).limit(1).get()

    return {
      rescueOrder: parseInt(input),
      hexId: snapshot.empty ? false : snapshot.docs[0].get('catId'),
    }
  }
}

export interface MoonCatSummary {
  catId: HexString
  rescueOrder: number
  collection: {
    name: string
    address?: HexString
  }
}

/**
 * Create an abbreviated data object for a MoonCat
 */
function summarizeMoonCat(doc: FirebaseFirestore.DocumentSnapshot): MoonCatSummary {
  let collection: MoonCatSummary['collection'] = {
    name: 'Unknown',
  }
  if (doc.get(`owner.${JumpPort.address}.value`) !== false) {
    collection = {
      name: 'JumpPort',
      address: JumpPort.address as HexString,
    }
  } else if (doc.get(`owner.${MoonCatRescue.address}.value`) == OldWrappedMoonCats.address) {
    // Old-wrapped MoonCat; report as Original rather than using the pass-through Acclimator value
    collection = {
      name: 'Original',
      address: MoonCatRescue.address as HexString,
    }
  } else if (doc.get(`owner.${MoonCatAcclimator.address}.value`) !== false) {
    collection = {
      name: 'Acclimated',
      address: MoonCatAcclimator.address as HexString,
    }
  } else if (doc.get(`owner.${MoonCatRescue.address}.value`) !== false) {
    collection = {
      name: 'Original',
      address: MoonCatRescue.address as HexString,
    }
  }

  return {
    catId: doc.get('catId'),
    rescueOrder: doc.get('rescueOrder'),
    collection,
  }
}

/**
 * Get all MoonCats owned by an address, by querying the local database cache of MoonCat metadata
 */
export const getOwnedMoonCats = async function ({
  collection,
  dbtx,
  targetAddress,
}: {
  collection: FirebaseFirestore.CollectionReference
  dbtx: FirebaseFirestore.Transaction
  targetAddress: HexString
}) {
  logger.debug(`Doing database lookup of all MoonCats for address ${targetAddress}...`)
  let moonCats: Array<MoonCatSummary> = []

  // Get all MoonCats owned by this address that are in the JumpPort
  let rs = await dbtx.get(
    collection.where(`owner.${JumpPort.address}.value`, '==', targetAddress).orderBy('rescueOrder')
  )
  logger.debug(`JumpPort MoonCats for ${targetAddress}: ${rs.size}`)
  moonCats = moonCats.concat(rs.docs.map(summarizeMoonCat))

  // Get all MoonCats owned by this address that are Acclimated
  // This query excludes all MoonCats deposited in the JumpPort, to keep from incorrectly attributing all
  // JumpPort-deposited MoonCats as owned by the JumpPort's address
  // And explicitly requires the MoonCatRescue owner to be the Acclimator, to avoid mis-assigning old-wrapped MoonCats
  // due to the Acclimator's pass-through ownership query
  rs = await dbtx.get(
    collection
      .where(`owner.${MoonCatRescue.address}.value`, '==', MoonCatAcclimator.address)
      .where(`owner.${MoonCatAcclimator.address}.value`, '==', targetAddress)
      .where(`owner.${JumpPort.address}.value`, '==', false)
      .orderBy('rescueOrder')
  )
  logger.debug(`Acclimated MoonCats for ${targetAddress}: ${rs.size}`)
  moonCats = moonCats.concat(rs.docs.map(summarizeMoonCat))

  // Get all MoonCats owned by this address that are in the original collection
  // This query excludes all Acclimated MoonCats, to keep from incorrectly attributing all
  // Acclimated MoonCats as owned by the Acclimator's address
  if (targetAddress == OldWrappedMoonCats.address) {
    // Because the Acclimator does a pass-through for ownership in the old wrapper, this edge case needs different logic
    rs = await dbtx.get(
      collection.where(`owner.${MoonCatRescue.address}.value`, '==', targetAddress).orderBy('rescueOrder')
    )
  } else {
    rs = await dbtx.get(
      collection
        .where(`owner.${MoonCatRescue.address}.value`, '==', targetAddress)
        .where(`owner.${MoonCatAcclimator.address}.value`, '==', false)
        .orderBy('rescueOrder')
    )
  }
  logger.debug(`MoonCats for ${targetAddress}: ${rs.size}`)
  moonCats = moonCats.concat(rs.docs.map(summarizeMoonCat))

  return moonCats
}

/**
 * From a Firebase document that is a MoonCat metadata record, return that MoonCat's owner's address.
 *
 * To determine the owner is more complicated than just looking up a property on the document, since different staking/wrapping
 * contracts need to be accounted for.
 */
export function getMoonCatOwnerFromDoc(moonCatDoc: DocumentSnapshot): BlockchainValue<HexString> | null {
  if (!moonCatDoc.exists) return null // Document doesn't actually exist in the Firebase store
  const moonCatData = moonCatDoc.data()
  if (typeof moonCatData == 'undefined') return null // Document exists, but has no data
  if (typeof moonCatData.owner === 'undefined') return null // Document exists, but has no ownership information

  if (moonCatData.owner[JumpPort.address] && moonCatData.owner[JumpPort.address].value !== false) {
    // JumpPort knows of this MoonCat; MoonCat is staked there and therefore what JumpPort reports as the owner is the true owner
    return moonCatData.owner[JumpPort.address]
  } else if (
    moonCatData.owner[MoonCatRescue.address] &&
    moonCatData.owner[MoonCatRescue.address].value == OldWrappedMoonCats.address
  ) {
    // Old-wrapped MoonCat
    return moonCatData.owner[MoonCatRescue.address]
  } else if (
    moonCatData.owner[MoonCatAcclimator.address] &&
    moonCatData.owner[MoonCatAcclimator.address].value !== false
  ) {
    // Acclimator knows of this MoonCat; MoonCat is Acclimated and therefore what the Acclimator reports as owner is the true owner
    return moonCatData.owner[MoonCatAcclimator.address]
  } else if (moonCatData.owner[MoonCatRescue.address] && moonCatData.owner[MoonCatRescue.address].value !== false) {
    // The original MoonCatRescue contract's ownership info is the true owner
    return moonCatData.owner[MoonCatRescue.address]
  }

  // Corrupted document; none of the MoonCatRescue-related contracts have ownership info for this MoonCat
  throw new Error('Unknown owner')
}

/**
 * Update dependent documents for CatRescued Events
 *
 * Called when a CatRescued Event from the MoonCatRescue contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatRescued: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { to: rescuer, catId: moonCatId } = evtLog.args as { to: HexString; catId: HexString }

  // MoonCat has been rescued; update the MoonCat's metadata record to note the rescuer
  const moonCatRef = db.collection(MOONCAT_COLLECTION).doc(moonCatId)
  const logPreamble = `CatRescued event seen for MoonCat ${moonCatId}`

  const moonCatData = (await dbtx.get(moonCatRef)).data()
  if (!moonCatData) {
    logger.error(`${logPreamble}, but no trait document exists for that MoonCat`)
    return
  }

  // Assemble an object for feeding into a Firestore "update" call, for this change
  let updateMoonCat: { [key: string]: any } = {}
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  updateMoonCat[`rescuer.value`] = rescuer
  updateMoonCat[`rescuer.modified`] = eventModified
  await dbtx.update(moonCatRef, updateMoonCat)
  logger.info(`${logPreamble}, rescued by ${rescuer}. MoonCat metadata updated.`)
}

/**
 * Update dependent documents for CatNamed Events
 *
 * Called when a CatNamed Event from the MoonCatRescue contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatNamed: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { catId: moonCatId, catName } = evtLog.args as { catId: HexString; catName: HexString }

  // MoonCat has been named; update the MoonCat's metadata record to note the new name
  const moonCatRef = db.collection(MOONCAT_COLLECTION).doc(moonCatId)
  const logPreamble = `CatNamed event seen for MoonCat ${moonCatId}`

  const moonCatData = (await dbtx.get(moonCatRef)).data()
  if (!moonCatData) {
    logger.error(`${logPreamble}, but no trait document exists for that MoonCat`)
    return
  }
  const { shouldClearChecked } = shouldUpdateBlockchainMoment(moonCatData.name, evtLog.blockNumber)

  // Assemble an object for feeding into a Firestore "update" call, for this change
  let updateMoonCat: { [key: string]: any } = {}
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  if (shouldClearChecked) updateMoonCat[`name.checked`] = FieldValue.delete()
  updateMoonCat[`name.nameRaw`] = catName
  updateMoonCat[`name.value`] = rawNameToString(catName)
  updateMoonCat[`name.isNamed`] = true
  updateMoonCat[`name.modified`] = eventModified
  await dbtx.update(moonCatRef, updateMoonCat)
  logger.info(`${logPreamble}, named as ${catName}. MoonCat metadata updated.`)
}

async function updateOwnershipRecord(
  dbtx: FirebaseFirestore.Transaction,
  evtLog: EventLog,
  moonCatRef: FirebaseFirestore.DocumentReference,
  logPreamble: string,
  ownershipKey: string,
  newOwner: any
) {
  const moonCatData = (await dbtx.get(moonCatRef)).data()
  if (!moonCatData) {
    logger.error(`${logPreamble}, but no trait document exists for that MoonCat`)
    return
  }

  const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
    moonCatData.owner[ownershipKey],
    evtLog.blockNumber
  )
  if (!shouldUpdateValue && !shouldUpdateModified && !shouldClearChecked) {
    logger.debug(
      `${logPreamble}, moving to ${newOwner}, but no update needed for that: ${JSON.stringify(
        moonCatData.owner[ownershipKey]
      )}`
    )
    return
  }

  // Assemble an object for feeding into a Firestore "update" call, for this change
  let updateMoonCat: { [key: string]: any } = {}
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  if (shouldClearChecked) updateMoonCat[`owner.${ownershipKey}.checked`] = FieldValue.delete()
  if (shouldUpdateValue) updateMoonCat[`owner.${ownershipKey}.value`] = newOwner
  if (shouldUpdateModified) updateMoonCat[`owner.${ownershipKey}.modified`] = eventModified
  await dbtx.update(moonCatRef, updateMoonCat)
  if (shouldUpdateValue) {
    logger.info(`${logPreamble}, moving to ${newOwner}. Metadata updated.`)
  } else if (shouldUpdateModified) {
    logger.debug(`${logPreamble}, moving to ${newOwner}. Updating last-modified record.`)
  }
}

/**
 * Update dependent documents for CatAdopted Events
 *
 * Called when a CatAdopted Event from the MoonCatRescue contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatAdopted: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { to: newOwner, catId: moonCatId } = evtLog.args as { to: HexString; catId: HexString }

  // MoonCat changed ownership; update the MoonCat's metadata record to note the new owner
  await updateOwnershipRecord(
    dbtx,
    evtLog,
    db.collection(MOONCAT_COLLECTION).doc(moonCatId),
    `CatAdopted event seen for MoonCat ${moonCatId}`,
    MoonCatRescue.address,
    newOwner
  )
}

/**
 * Update dependent documents for Transfer Events
 *
 * Called when a Transfer Event from the MoonCatAcclimator contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatTransferred: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { to: newOwner, tokenId: rescueOrder } = evtLog.args as { from: HexString; to: HexString; tokenId: number }

  // Acclimated MoonCat changed ownership; update the MoonCat's metadata record to note the new owner
  const snapshot = await dbtx.get(db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', rescueOrder).limit(1))
  if (snapshot.empty) {
    logger.error(`Transfer event seen for MoonCat ${rescueOrder}, but no trait document exists for that MoonCat`)
    return
  }
  const ZERO_ADDRESS: `0x${string}` = '0x0000000000000000000000000000000000000000'
  const formattedOwner = newOwner == ZERO_ADDRESS ? false : newOwner

  await updateOwnershipRecord(
    dbtx,
    evtLog,
    snapshot.docs[0].ref,
    `Transfer event seen for MoonCat ${rescueOrder}`,
    MoonCatAcclimator.address,
    formattedOwner
  )
}

/**
 * Update dependent documents for ReceivedChild Events
 *
 * Called when a ReceivedChild Event from the MoonCatAcclimator contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatChildReceived: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { _toTokenId, _childContract, _childTokenId } = evtLog.args as {
    _from: HexString
    _toTokenId: number
    _childContract: HexString
    _childTokenId: number
  }

  switch (_childContract) {
    case MoonCatAcclimator.address: {
      // A MoonCat has been moved to the Purrse of another MoonCat

      const snapshot = await dbtx.get(
        db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', _childTokenId).limit(1)
      )
      if (snapshot.empty) {
        logger.error(
          `ReceivedChild event seen that moves MoonCat ${_childTokenId}, but no trait document exists for that MoonCat`
        )
        return
      }

      await updateOwnershipRecord(
        dbtx,
        evtLog,
        snapshot.docs[0].ref,
        `ReceivedChild event seen that moves MoonCat ${_childTokenId}`,
        MoonCatAcclimator.address + '-998',
        _toTokenId
      )
      break
    }
    case MOMENTS_ADDRESS: {
      // A MoonCatMoment has been given to a MoonCat
      await updateOwnershipRecord(
        dbtx,
        evtLog,
        db.collection(MOMENTS_COLLECTION).doc(String(_childTokenId)),
        `ReceivedChild event seen that moves Moment ${_childTokenId}`,
        MoonCatAcclimator.address + '-998',
        _toTokenId
      )
      break
    }
    default:
      logger.info(`ReceivedChild event seen for contract ${_childContract}, which has no post-action`)
  }
}

/**
 * Update dependent documents for TransferChild Events
 *
 * Called when a TransferChild Event from the MoonCatAcclimator contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postMoonCatChildTransferred: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { _childContract, _childTokenId } = evtLog.args as {
    _fromTokenId: number
    _to: HexString
    _childContract: HexString
    _childTokenId: number
  }

  if (_childContract == MoonCatAcclimator.address) {
    // A MoonCat has been moved out of the Purrse of another MoonCat
    const snapshot = await dbtx.get(
      db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', _childTokenId).limit(1)
    )
    if (snapshot.empty) {
      logger.error(
        `TransferChild event seen that moves MoonCat ${_childTokenId}, but no trait document exists for that MoonCat`
      )
      return
    }

    await updateOwnershipRecord(
      dbtx,
      evtLog,
      snapshot.docs[0].ref,
      `TransferChild event seen that moves MoonCat ${_childTokenId}`,
      MoonCatAcclimator.address + '-998',
      false
    )
  } else if (_childContract == MOMENTS_ADDRESS) {
    // A MoonCatMoment has been removed from a MoonCat
    await updateOwnershipRecord(
      dbtx,
      evtLog,
      db.collection(MOMENTS_COLLECTION).doc(String(_childTokenId)),
      `TransferChild event seen that moves Moment ${_childTokenId}`,
      MoonCatAcclimator.address + '-998',
      false
    )
  } else {
    logger.info(`TransferChild event seen for contract ${_childContract}, which has no post-action`)
  }
}
