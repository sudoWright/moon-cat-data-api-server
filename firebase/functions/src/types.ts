import { ethers } from 'ethers'
import { Timestamp } from 'firebase-admin/firestore'

export type HexString = `0x${string}`

type DateTimeString = string
export function isDateTimeString(msg: string): msg is DateTimeString {
  return !isNaN(Date.parse(msg))
}
export function isHexString(msg?: string): msg is HexString {
  return ethers.utils.isHexString(msg)
}

export interface BlockchainMoment {
  txHash?: `0x${string}`
  blockHeight: number
  timestamp?: Timestamp
}
export interface BlockchainValue<T = any> {
  value: T
  modified?: BlockchainMoment
  checked?: BlockchainMoment
}
