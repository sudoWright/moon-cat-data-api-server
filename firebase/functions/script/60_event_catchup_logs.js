require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Browse a listing of recent tasks that have been run on the server.
 */
;(async () => {
  const snapshot = await db.collection('task-logs').orderBy('startTime', 'desc').limit(50).get()

  snapshot.docs.forEach((log) => {
    const data = log.data()
    const delta = (data.endTime.toMillis() - data.startTime.toMillis()) / 1000
    data.startTime = data.startTime.toDate().toISOString()
    data.endTime = data.endTime.toDate().toISOString()

    console.log(
      `${data.startTime}: ${data.taskName} ran for ${delta}s -> found ${data.logsProcessed} logs between blocks ${data.startBlock} and ${data.endBlock}. ${data.logsUpdated} logs updated`
    )
  })
})()
