require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const { doMulticall, sleep } = require('./lib/contractEventsUtil')
const { rawNameToString, MOONCAT_COLLECTION } = require('../lib/moonCatUtils')

const { MoonCatRescue, MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const MOONCAT_START_AT = 0

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Query MoonCat trait data from the blockchain and update the data in Firebase
 *
 * This script iterates through all the MoonCats the Firestore database knows of, and checks the current status of
 * traits that can change (ownership and named status).
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log('latest block', latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  console.log('Updating MoonCats...')
  const CHUNK_SIZE = 10
  // Find the MoonCat to start at
  let mcSnap = await db
    .collection(MOONCAT_COLLECTION)
    .where('rescueOrder', '<=', MOONCAT_START_AT)
    .orderBy('rescueOrder', 'desc')
    .limit(3)
    .get()
  if (mcSnap.empty) {
    console.log(mcSnap)
    console.error('Cannot find MoonCat with rescue order of', MOONCAT_START_AT)
    process.exit(1)
  }
  let moonCatCursor = mcSnap.docs[0]
  const query = db.collection(MOONCAT_COLLECTION).orderBy('rescueOrder').limit(CHUNK_SIZE)
  let chunk = await query.startAt(moonCatCursor).get() // First chunk is starting AT the cursor

  while (!chunk.empty) {
    console.log(`At MoonCat ${moonCatCursor.get('rescueOrder')}...`)
    let output = []
    let calls = []
    for (let index in chunk.docs) {
      const ref = chunk.docs[index]
      output[index] = { catId: ref.get('catId') }
      calls.push({
        id: 'name',
        index: index,
        contract: MoonCatRescue,
        method: 'catNames',
        args: [ref.get('catId')],
      })
      calls.push({
        id: 'rescue-owner',
        index: index,
        contract: MoonCatRescue,
        method: 'catOwners',
        args: [ref.get('catId')],
      })
      calls.push({
        id: 'acclimated-owner',
        index: index,
        contract: MoonCatAcclimator,
        method: 'ownerOf',
        args: [ref.get('rescueOrder')],
      })
      calls.push({
        id: 'acclimated-child-token',
        index: index,
        contract: MoonCatAcclimator,
        method: 'ownerOfChild',
        args: [MoonCatAcclimator.address, ref.get('rescueOrder')],
      })
      calls.push({
        id: 'jumpport-owner',
        index: index,
        contract: JumpPort,
        method: 'ownerOf',
        args: [MoonCatAcclimator.address, ref.get('rescueOrder')],
      })
    }
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'name':
          if (rs.parsed[0] == '0x0000000000000000000000000000000000000000000000000000000000000000') {
            output[rs.index][`name.isNamed`] = false
            output[rs.index][`name.value`] = false
            output[rs.index][`name.checked`] = checked
          } else {
            output[rs.index][`name.isNamed`] = true
            output[rs.index][`name.nameRaw`] = rs.parsed[0]
            output[rs.index][`name.value`] = rawNameToString(rs.parsed[0])
            output[rs.index][`name.checked`] = checked
          }
          break
        case 'rescue-owner':
          output[rs.index][`owner.${MoonCatRescue.address}.checked`] = checked
          output[rs.index][`owner.${MoonCatRescue.address}.value`] = rs.parsed[0]
          break
        case 'acclimated-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${MoonCatAcclimator.address}.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}.value`] = false
          } else {
            output[rs.index][`owner.${MoonCatAcclimator.address}.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}.value`] = rs.parsed[0]
          }
          break
        case 'acclimated-child-token':
          if (!rs.success) {
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.value`] = false
          } else {
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.value`] = rs.parsed.parentTokenId.toNumber()
          }
          break
        case 'jumpport-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = false
          } else {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = rs.parsed[0]
          }
          break
      }
    }

    const batch = db.batch()
    for (o of output) {
      batch.update(db.collection(MOONCAT_COLLECTION).doc(o.catId), o, { merge: true })
    }
    await batch.commit()
    await sleep(CHUNK_SIZE / 5) // After batch-comit is done, wait some time for "onUpdate" hooks to run

    // Grab next chunk
    moonCatCursor = chunk.docs[chunk.docs.length - 1]
    chunk = await query.startAfter(moonCatCursor).get() // Subsequent chunks start AFTER the cursor
  }

  console.log('Done!')
})()
