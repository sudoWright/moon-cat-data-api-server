require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, FieldPath } = require('firebase-admin/firestore')

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Loop through all Address metadata records and ensure that all have a 'checked' field.
 *
 * Firestore does not allow for searching for records that are missing a specific field.
 * The only way to find them is to iterate over every document in the collection. In this
 * situation, we add the field to those that we find that are missing it.
 */
;(async () => {
  let query = db.collection('address-meta').orderBy(FieldPath.documentId()).limit(50)
  let pageCursor = false
  let recordsChanged = 0
  while (true) {
    if (pageCursor != false) query = query.startAfter(pageCursor)
    const snapshot = await query.get()

    for (addrSnap of snapshot.docs) {
      if (typeof addrSnap.get('checked') == 'undefined') {
        console.log('Updating:', addrSnap.id)
        await addrSnap.ref.set({ checked: null }, { merge: true })
        recordsChanged++
      }
      pageCursor = addrSnap.ref
    }

    if (recordsChanged > 200) return
    console.log('Fetching another batch of address records...')
  }
})()
