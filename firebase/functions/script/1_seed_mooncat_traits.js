require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const { doMulticall, sleep } = require('./lib/contractEventsUtil')
const { rawNameToString, MOONCAT_COLLECTION } = require('../lib/moonCatUtils')

const { MoonCatRescue, MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)

const MOONCAT_START_AT = 0
const TRAITS = new ethers.Contract(
  '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2',
  [
    'function traitsOf (uint256 rescueOrder) public view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose, bytes5 catId, uint16 rescueYear, bool isNamed)',
    'function nameOf (uint256 rescueOrder) public view returns (string name)',
  ],
  provider
)

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Query MoonCat trait data from the blockchain and insert into Firebase
 *
 * Use the MoonCats on-chain contract to enumerate human-friendly labels for
 * each MoonCat, and query the MoonCatRescue, Acclimator, and JumpPort contracts
 * to record who owns that MoonCat at the moment.
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log('latest block', latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  // Insert MoonCat trait data into the “mooncats” collection
  console.log('Adding MoonCats...')
  const CHUNK_SIZE = 50
  for (let i = MOONCAT_START_AT; i < 25440; i += CHUNK_SIZE) {
    console.log(`At MoonCat ${i}...`)
    let calls = []
    for (let j = 0; j < CHUNK_SIZE; j++) {
      const rescueOrder = i + j
      if (rescueOrder >= 25440) break
      calls.push({
        id: 'traits',
        index: j,
        contract: TRAITS,
        method: 'traitsOf',
        args: [rescueOrder],
      })
      calls.push({
        id: 'acclimated-owner',
        index: j,
        contract: MoonCatAcclimator,
        method: 'ownerOf',
        args: [rescueOrder],
      })
      calls.push({
        id: 'acclimated-child-token',
        index: j,
        contract: MoonCatAcclimator,
        method: 'ownerOfChild',
        args: [MoonCatAcclimator.address, rescueOrder],
      })
      calls.push({
        id: 'jumpport-owner',
        index: j,
        contract: JumpPort,
        method: 'ownerOf',
        args: [MoonCatAcclimator.address, rescueOrder],
      })
    }
    let output = []
    let subCalls = []
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'traits':
          output[rs.index] = {
            rescueOrder: rs.index + i,
            catId: rs.parsed.catId,
            genesis: rs.parsed.genesis,
            pale: rs.parsed.pale,
            facing: rs.parsed.facing,
            expression: rs.parsed.expression,
            pattern: rs.parsed.pattern,
            pose: rs.parsed.pose,
            rescueYear: rs.parsed.rescueYear,
            name: {
              checked: checked,
              value: false,
              isNamed: rs.parsed.isNamed,
            },
            owner: {},
          }
          subCalls.push({
            id: 'rescue-owner',
            index: rs.index,
            contract: MoonCatRescue,
            method: 'catOwners',
            args: [rs.parsed.catId],
          })
          if (rs.parsed.isNamed) {
            subCalls.push({
              id: 'name-raw',
              index: rs.index,
              contract: MoonCatRescue,
              method: 'catNames',
              args: [rs.parsed.catId],
            })
          }
          break
        case 'acclimated-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index].owner[MoonCatAcclimator.address] = {
              value: false,
              checked: checked,
            }
          } else {
            output[rs.index].owner[MoonCatAcclimator.address] = {
              value: rs.parsed[0],
              checked: checked,
            }
          }
          break
        case 'acclimated-child-token':
          if (!rs.success) {
            output[rs.index].owner[MoonCatAcclimator.address + '-998'] = {
              value: false,
              checked: checked,
            }
          } else {
            output[rs.index].owner[MoonCatAcclimator.address + '-998'] = {
              value: rs.parsed[1],
              checked: checked,
            }
          }
          break
        case 'jumpport-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index].owner[JumpPort.address] = {
              value: false,
              checked: checked,
            }
          } else {
            output[rs.index].owner[JumpPort.address] = {
              value: rs.parsed[0],
              checked: checked,
            }
          }
          break
        default:
          console.error('Unknown result ID', rs)
          process.exit(1)
      }
    }

    for (let rs of await doMulticall(subCalls)) {
      switch (rs.id) {
        case 'name-raw':
          output[rs.index].name.nameRaw = rs.parsed[0]
          output[rs.index].name.value = rawNameToString(rs.parsed[0])
          break
        case 'rescue-owner':
          output[rs.index].owner[MoonCatRescue.address] = {
            value: rs.parsed[0],
            checked: checked,
          }
          break
        default:
          console.error('Unknown result ID', rs)
          process.exit(1)
      }
    }

    const batch = db.batch()
    for (o of output) {
      batch.set(db.collection(MOONCAT_COLLECTION).doc(o.catId), o, { merge: true })
    }
    await batch.commit()
    await sleep(CHUNK_SIZE / 5) // After batch-comit is done, wait some time for "onUpdate" hooks to run

  }
  console.log('Done!')
})()
