require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const { processEventLog, CONFIG_COLLECTION, CONFIG_DOC } = require('../lib/logUtils')
const { getEventsForContract, getStartBlock } = require('./lib/contractEventsUtil')
const { MOMENTS_ADDRESS } = require('../lib/momentUtils')

const { API_URL } = process.env
const provider = new ethers.providers.JsonRpcProvider(API_URL)
const MOMENTS = new ethers.Contract(
  MOMENTS_ADDRESS,
  ['event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)'],
  provider
)
const START_BLOCK = 13850240

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

function markCompletedHeight(eventName, blockHeight) {
  let keyName = `${MOMENTS.address}-${eventName}`
  return db
    .collection(CONFIG_COLLECTION)
    .doc(CONFIG_DOC)
    .set({ [keyName]: blockHeight }, { merge: true })
}

/**
 * Query MoonCatMoment event data from the blockchain and insert into Firebase
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  // Insert MoonCatMoment Events into the EVENTS_COLLECTION collection
  for (eventName of ['Transfer']) {
    const startBlock = await getStartBlock(`${MOMENTS.address}-${eventName}`, db, START_BLOCK)
    console.log(`Adding ${eventName} events from ${startBlock}...`)
    let lastBlockHeight = startBlock

    // Log processing needs to happen in order. This process does not use a queue for parallel execution
    // because if there are multiple Events in the same transaction, the last Event should be the one that
    // is marked as the "last-modified" value. If Events are processed in parallel, there's a chance of a race
    // condition. Hence each Event is committed to the data store before moving on to process the next Event.
    for await (const { log, index } of getEventsForContract(MOMENTS, startBlock, eventName, 20_000)) {
      lastBlockHeight = log.blockNumber

      await processEventLog(db, MOMENTS, log)

      if (index > 0 && index % 10 == 0) console.log(`At ${index}...`)
      if (index > 0 && index % 50 == 0) {
        await markCompletedHeight(eventName, log.blockNumber)
      }
    }
    await markCompletedHeight(eventName, lastBlockHeight)
    console.log('Done!')
  }
})()
