require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore, FieldPath, Timestamp } = require('firebase-admin/firestore')
const { ethers } = require('ethers')
const { doMulticall, sleep } = require('./lib/contractEventsUtil')

const { MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils')
const { MOMENTS_COLLECTION, MOMENTS_ADDRESS } = require('../lib/momentUtils')

const { API_URL } = process.env
if (API_URL == '') {
  console.error('No RPC API endpoint set')
  process.exit(1)
}
const provider = new ethers.providers.JsonRpcProvider(API_URL)
const MOMENTS = new ethers.Contract(
  MOMENTS_ADDRESS,
  ['function ownerOf(uint256 tokenId) external view returns (address)'],
  provider
)

const MOMENT_START_AT = 0

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

/**
 * Query MoonCatMoment trait data from the blockchain and update the data in Firebase
 *
 * This script iterates through all the Moments the Firestore database knows of, and checks the current status of
 * traits that can change (ownership).
 */
;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const latestBlock = await provider.getBlock('latest')
  console.log('latest block', latestBlock.number)
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }

  console.log('Updating Moments...')
  const CHUNK_SIZE = 10
  // Find the Moment to start at
  console.log(MOMENTS_COLLECTION, MOMENT_START_AT)
  let momentCursor = await db.collection(MOMENTS_COLLECTION).doc(String(MOMENT_START_AT)).get()
  if (!momentCursor.exists) {
    console.log(momentCursor)
    console.error('Cannot find Moment with ID of', MOMENT_START_AT)
    process.exit(1)
  }
  const query = db.collection(MOMENTS_COLLECTION).orderBy(FieldPath.documentId()).limit(CHUNK_SIZE)
  let chunk = await query.startAt(momentCursor).get() // First chunk is starting AT the cursor

  while (!chunk.empty) {
    console.log(`At Moment ${momentCursor.id}...`)
    let output = []
    let calls = []
    for (let index in chunk.docs) {
      const ref = chunk.docs[index]
      output[index] = { id: ref.id }

      calls.push({
        id: 'moment-owner',
        index: index,
        contract: MOMENTS,
        method: 'ownerOf',
        args: [ref.id],
      })
      calls.push({
        id: 'acclimated-child-token',
        index: index,
        contract: MoonCatAcclimator,
        method: 'ownerOfChild',
        args: [MOMENTS_ADDRESS, ref.id],
      })
      calls.push({
        id: 'jumpport-owner',
        index: index,
        contract: JumpPort,
        method: 'ownerOf',
        args: [MOMENTS_ADDRESS, ref.id],
      })
    }
    for (let rs of await doMulticall(calls)) {
      switch (rs.id) {
        case 'moment-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${MOMENTS_ADDRESS}.checked`] = checked
            output[rs.index][`owner.${MOMENTS_ADDRESS}.value`] = false
          } else {
            output[rs.index][`owner.${MOMENTS_ADDRESS}.checked`] = checked
            output[rs.index][`owner.${MOMENTS_ADDRESS}.value`] = rs.parsed[0]
          }
          break
        case 'acclimated-child-token':
          if (!rs.success) {
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.value`] = false
          } else {
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.checked`] = checked
            output[rs.index][`owner.${MoonCatAcclimator.address}-998.value`] = rs.parsed.parentTokenId.toNumber()
          }
          break
        case 'jumpport-owner':
          if (!rs.success || rs.parsed[0] == ethers.constants.AddressZero) {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = false
          } else {
            output[rs.index][`owner.${JumpPort.address}.checked`] = checked
            output[rs.index][`owner.${JumpPort.address}.value`] = rs.parsed[0]
          }
          break
      }
    }

    let t = sleep(CHUNK_SIZE / 5)
    // Do a batch-update
    const batch = db.batch()
    for (o of output) {
      const update = Object.assign({}, o)
      delete update.id
      batch.set(db.collection(MOMENTS_COLLECTION).doc(o.id), update, { merge: true })
    }
    await batch.commit()
    await t // Make sure script doesn't run faster than prescribed rate, to allow Firestore post-update hooks to not get backlogged

    // Grab next chunk
    momentCursor = chunk.docs[chunk.docs.length - 1]
    chunk = await query.startAfter(momentCursor).get() // Subsequent chunks start AFTER the cursor
  }

  console.log('Done!')
})()
