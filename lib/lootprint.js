require('dotenv').config();
const { API_URL } = process.env;
const LibMoonCat = require('libmooncat');
const Cache = require('./cache');

const CACHE_TYPE = 'lootprint';

class Lootprint {
  constructor(lootprintId) {
    this.lootprintId = lootprintId;
    this.data = null;
  }

  async init() {
    console.log(`Initialising Lootprint Data: ${this.lootprintId}`);
    this.data = await LibMoonCat.getLootprint(this.lootprintId);
  }

  hasLootprint() {
    return this.data !== null;
  }

  getTrait() {
    return {
      trait_type: 'lootprint',
      value: this.hasLootprint() ? 'Minted' : 'Lost',
    };
  }

  static factory(lootprintId) {
    if (!Cache.isFresh(CACHE_TYPE, lootprintId) && !Cache.isPending(CACHE_TYPE, lootprintId)) {
      Cache.addItemAsync(CACHE_TYPE, lootprintId, new Promise(async (resolve, reject) => {
        try {
          let loot = new Lootprint(lootprintId);
          await loot.init();
          resolve(loot);
        } catch (err) {
          reject(err);
        }
      }));
    }

    return Cache.getItem(CACHE_TYPE, lootprintId);
  }

  static getAge(catId) {
    return Cache.itemAge(CACHE_TYPE, catId);
  }
}

module.exports = Lootprint;
