const { ethers } = require('ethers');
const LibMoonCat = require('libmooncat');
const { API_URL } = process.env;

const provider = new ethers.providers.JsonRpcProvider(API_URL);
let moonCatOwners = require('../data/mooncat_owners.json');
let moonCatAcclimatedOwners = require('../data/mooncat-acclimated_owners.json');
let moonCatJumpPortOwners = require('../data/mooncat-jumpport_owners.json');

/**
 * Given a contract interface and an event signature,
 * start listening for those sort of events to happen,
 * and return a function that can manually update from a specific block height.
 */
async function ownershipFactory({label, meta, topics, logHandler, catchUpFrom}) {
  const CONTRACT = new ethers.Contract(meta.address, meta.abi, provider);
  const filter = {
    address: meta.address,
    topics: topics,
  }

  async function catchUp(startBlock) {
    //console.debug('Catching up from block', startBlock)
    let logs = await CONTRACT.queryFilter(filter, startBlock)
    console.debug(`${label}: ${logs.length} logs to catch up on...`)
    logs.forEach(logHandler);
    console.debug(`${label}: Up-to-date`);
    return logs.length;
  }
  // If a catch-up-from block is provided, do that catch-up first (await it),
  // before starting to live-listen for events
  if (typeof catchUpFrom != 'undefined') {
    await catchUp(catchUpFrom);
  }

  // Watch for any new occurrences of this filter
  provider.on(filter, (log => {
    //console.debug(`Filter handler for ${label} fired`);
    // The log object passed into a live-polling listener like this is a raw event.
    // Parse it with the contract, to name the arguments of the Event.
    let parsedLog = CONTRACT.interface.parseLog(log);
    log.args = parsedLog.args;
    log.name = parsedLog.name;
    log.signature = parsedLog.signature;
    logHandler(log);
  }));

  return catchUp
}

async function init() {
  /**
   * Track ownership information for original MoonCats from on-chain
   */
  await ownershipFactory({
    label: 'MoonCatRescue',
    meta: LibMoonCat.contracts.MoonCatRescue,
    topics: [ethers.utils.id('CatAdopted(bytes5,uint256,address,address)')],
    logHandler: (log) => {
      let rescueOrder = LibMoonCat.getRescueOrder(log.args.catId);
      //console.debug(`Block ${log.blockNumber} - MoonCat ${rescueOrder} (${log.args.catId}) => ownership to ${log.args.to}`);
      if (log.blockNumber < moonCatOwners[rescueOrder].block) {
        console.log(`Event is too old (${log.blockNumber}, but we're updated through ${moonCatOwners[rescueOrder].block})`);
        return;
      }
      moonCatOwners[rescueOrder] = {
        owner: log.args.to,
        block: log.blockNumber
      };
    },
    catchUpFrom: moonCatOwners[0].block
  });

  /**
   * Track ownership of Acclimated/wrapped MoonCats
   */
  await ownershipFactory({
    label: 'Acclimated MoonCats',
    meta: LibMoonCat.contracts.MoonCatAcclimator,
    topics: [ethers.utils.id('Transfer(address,address,uint256)')],
    logHandler: (log) => {
      let catId = LibMoonCat.getCatId(log.args.tokenId);
      //console.debug(`Block ${log.blockNumber} - Acclimated MoonCat ${log.args.tokenId} (${catId}) => ownership to ${log.args.to}`);
      if (log.blockNumber < moonCatAcclimatedOwners[log.args.tokenId].block) {
        console.log(`Event is too old (${log.blockNumber}, but we're updated through ${moonCatAcclimatedOwners[log.args.tokenId].block})`);
        return;
      }
      moonCatAcclimatedOwners[log.args.tokenId] = {
        owner: log.args.to,
        block: log.blockNumber
      };
    },
    catchUpFrom: moonCatAcclimatedOwners[0].block
  })

  /**
   * Track ownership of MoonCats in the JumpPort
   */
  await ownershipFactory({
    label: 'JumpPort MoonCats',
    meta: {
      address: '0xF4d150F3D03Fa1912aad168050694f0fA0e44532',
      abi: [
        'event Deposit(address indexed owner, address indexed tokenAddress, uint256 indexed tokenId)',
        'event Withdraw(address indexed owner, address indexed tokenAddress, uint256 indexed tokenId, uint256 duration)',
      ]
    },
    topics: [
      [ethers.utils.id('Deposit(address,address,uint256)'), ethers.utils.id('Withdraw(address,address,uint256,uint256')],
      null,
      ethers.utils.defaultAbiCoder.encode(['address'], [LibMoonCat.contracts.MoonCatAcclimator.address])
    ],
    logHandler: (log) => {
      switch(log.name) {
        case 'Deposit': {
          const catId = LibMoonCat.getCatId(log.args.tokenId);
          console.log(`Block ${log.blockNumber} - Deposited into JumpPort MoonCat ${log.args.tokenId} (${catId}) => ownership to ${log.args.owner}`)
          if (log.blockNumber < moonCatJumpPortOwners[log.args.tokenId].block) {
            console.log(`Event is too old (${log.blockNumber}, but we're updated through ${moonCatJumpPortOwners[log.args.tokenId].block})`);
            return;
          }
          moonCatJumpPortOwners[log.args.tokenId] = {
            owner: log.args.owner,
            block: log.blockNumber
          };
          return;
        }
        case 'Withdraw': {
          const catId = LibMoonCat.getCatId(log.args.tokenId);
          console.log(`Block ${log.blockNumber} - Withdrawn from JumpPort MoonCat ${log.args.tokenId} (${catId})`)
          if (log.blockNumber < moonCatJumpPortOwners[log.args.tokenId].block) {
            console.log(`Event is too old (${log.blockNumber}, but we're updated through ${moonCatJumpPortOwners[log.args.tokenId].block})`);
            return;
          }
          moonCatJumpPortOwners[log.args.tokenId] = {
            owner: null,
            block: log.blockNumber
          };
          return;
        }
        default: {
          console.log('JumpPort', log);
        }
      }
    },
    catchUpFrom: moonCatJumpPortOwners[0].block
  })
}
init();

function filterOwnersByAddress(ownersArray, ownerAddress) {
  return Object.keys(ownersArray).filter(rescueOrder => {
    if (ownersArray[rescueOrder].owner == null) return false;
    return ownersArray[rescueOrder].owner.toLowerCase() == ownerAddress;
  }).map(rescueOrder => Number(rescueOrder));
}

/**
 * For the specified Ethereum address, look up which MoonCats they own
 */
function ownedMoonCatsForAddress(ownerAddress) {
  console.log(`Getting MoonCats owned by ${ownerAddress}:`)
  const addr = ownerAddress.toLowerCase();

  function mapRescueOrderFactory(collectionData) {
    return function(rescueOrder) {
      return {
        rescueOrder: rescueOrder,
        catId: LibMoonCat.getCatId(rescueOrder),
        collection: collectionData
      }
    }
  }

  const m1 = filterOwnersByAddress(moonCatOwners, addr).map(mapRescueOrderFactory({
    name: 'Original',
    address: LibMoonCat.contracts.MoonCatRescue.address
  }));
  const m2 = filterOwnersByAddress(moonCatAcclimatedOwners, addr).map(mapRescueOrderFactory({
    name: 'Acclimated',
    address: LibMoonCat.contracts.MoonCatAcclimator.address
  }));
  const m3 = filterOwnersByAddress(moonCatJumpPortOwners, addr).map(mapRescueOrderFactory({
    name: 'JumpPort',
    address: '0xF4d150F3D03Fa1912aad168050694f0fA0e44532'
  }))

  return m1.concat(m2, m3).sort((a, b) => a.rescueOrder - b.rescueOrder);
}

module.exports = {
  ownedMoonCatsForAddress
}