require('dotenv').config();
const { CONFIG_FILENAME } = process.env;
const LibMoonCat = require('libmooncat');
const MoonCat = require('./mooncat');
const Accessory = require('./accessory');
const Owners = require('./owners');
const Cache = require('./cache');
const Lootprint = require('./lootprint');
const Config = require('../' + CONFIG_FILENAME);

const DEFAULT_SCALE = 5;
const DEFAULT_PADDING = DEFAULT_SCALE * 4;
const DEFAULT_GLOW_OPACITY = '1';

class Response {
  constructor(type, id) {
    if (typeof id === 'string') {
      let dotpos = id.indexOf('.');
      if (dotpos >= 0) {
        this.requestId = id.substring(0, dotpos);
      } else {
        this.requestId = id;
      }
    } else {
      this.requestId = id;
    }
    this.type = type || 'mooncat';

    if (type === 'mooncat') {
      // If the identifier requested is less than 25440, the request must be by rescue order and not hex ID
      // This works because no rescued MoonCat has a hex ID of 0x0000006360 or less
      if (Number(this.requestId) < 25440) this.catId = LibMoonCat.getCatId(Number(this.requestId));
      else this.catId = this.requestId;

      this.rescueOrder = LibMoonCat.getRescueOrder(this.catId);
      console.info('Initialized request for MoonCat', this.catId, this.rescueOrder);
    }
  }

  getAge() {
    switch(this.type) {
      case 'mooncat':
        return MoonCat.getAge(this.catId)
      case 'accessory':
        return Accessory.getAge(this.requestId)
      case 'lootprint':
        return Lootprint.getAge(this.requestId);
    }
    return null;
  }

  getCatId() {
    return this.catId;
  }

  getRescueOrder() {
    return this.rescueOrder;
  }

  async getTraits() {
    try {
      let mooncat = await MoonCat.factory(this.catId);
      return await mooncat.getTraits();
    } catch (err) {
      return err;
    }
  }

  async getContractDetails() {
    try {
      let mooncat = await MoonCat.factory(this.catId);
      return await mooncat.getContractDetails();
    } catch (err) {
      return err;
    }
  }

  async getContractDetailsSet(ids) {
    let output = [];
    let exists = [];

    try {
      if (typeof ids === 'object' && ids.length > 0) {
        let len = ids.length > 100 ? 100 : ids.length;
        for (let i = 0; i < len; i++) {
          let catId =
            Number(ids[i]) < 25440
              ? LibMoonCat.getCatId(Number(ids[i]))
              : ids[i];

          if (!exists.includes(catId)) {
            let mooncat = await MoonCat.factory(catId);
            output.push(await mooncat.getContractDetails());
          }

          exists.push(catId);
        }
      }
    } catch (err) {}

    return output;
  }

  async getDynamicHtml() {
    try {
      let mooncat = await MoonCat.factory(this.catId);
      return await mooncat.getDynamicHtml();
    } catch (err) {
      return err;
    }
  }

  async getImageData(options) {
    try {
      options = options || {};
      let mooncat = await MoonCat.factory(this.catId);

      return await mooncat.getImageData({
        scale: DEFAULT_SCALE,
        padding: DEFAULT_PADDING,
        glowOpacity: DEFAULT_GLOW_OPACITY,
        ...options,
      });
    } catch (err) {
      console.log(`Issue generating image for MoonCat ${this.requestId}`);
      return err;
    }
  }

  async getAccessory() {
    try {
      let acc = await Accessory.factory(this.requestId);
      return acc.getFullTraits();
    } catch (err) {
      return err;
    }
  }

  async getAccessoryImageData() {
    try {
      let acc = await Accessory.factory(this.requestId);

      return LibMoonCat.generateImage(
        acc.getRandomCatId(),
        [acc.getDrawable(0, 1)],
        {
          scale: DEFAULT_SCALE,
          padding: DEFAULT_PADDING,
        }
      );
    } catch (err) {
      return err;
    }
  }

  getImgFromData(base64Data) {
    return Buffer.from(
      base64Data.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''),
      'base64'
    );
  }

  getOwnedMooncats() {
    try {
      return Owners.ownedMoonCatsForAddress(this.requestId);
    } catch (err) {
      return err;
    }
  }

  getCostumes() {
    try {
      return Config.accessories.costumes;
    } catch (err) {
      return err;
    }
  }

  invalidateCacheItem(type, key) {
    try {
      // Set the item to expire right now. That will leave the old data being served,
      // but the next request for that item will attempt to refresh it.
      Cache.setItemTtl(type, key, 0);
    } catch (err) {
      return err;
    }
  }
}

module.exports = Response;
