const { BASE_URL, CONFIG_FILENAME } = process.env;
const Config = require('../' + CONFIG_FILENAME);

const chevronClosed =
  '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAGklEQVQImWNgoAjcvXv3Pz4+XABDAqcOsgEAhBAR763jivsAAAAASUVORK5CYII=">';
const chevronOpen =
  '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAG0lEQVQImWNgIBbcvXv3P14JDAXoAjhNoA4AALwZEe8knRByAAAAAElFTkSuQmCC">';

function accessoryItem(accessory) {
  let visible = accessory.zIndex > 0 && accessory.verified;

  return `<a target='_blank' href='${Config.urls.boutique}/a/${
    accessory.accessoryId
  }'>
    <span class='v${visible ? ' y' : ''}' title='${
    visible ? 'Visible' : 'Hidden'
  }'></span>#${accessory.accessoryId} ${accessory.name}</a>`;
}

function accessoryList(catId, accessories) {
  if (accessories.length === 0) return '';

  return `<div class='i'><img id='p' style='display:none;'  src='${BASE_URL}/image/${catId}'></div>
    <button id='t'>Show Accessories</button>
    <div id='a' class='i' style='display:none;'>
      <button id='b'>Owned Accessories ${chevronClosed}</button>
      <div style='display:none;' id='l'>
        ${accessories.map((accessory) => accessoryItem(accessory)).join('\n')}
      </div>
    </div>`;
}

module.exports = function view(catId, glowTotal, accessories) {
  let bgColor = glowTotal < 100 ? '#333' : '#111';

  return `<html><head><meta charset='utf8'><meta name='viewport' content='width=device-width'><title>${catId}</title>
  <link rel='stylesheet' href='https://mooncat.community/css/fonts.css'>
  <style>
    body{background-color:${bgColor};}
    #c{display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:center;align-content:center;align-items:center;min-height:95vh}
    .i{flex: 0 1 auto;align-self:auto;text-align:center;}
    img{max-width:100%;}
    button{border:none;background-color:rgba(0,0,0,0.4);padding:1px 5px;color:#ddd;border-radius:4px;cursor:pointer;font-family:screen1;}
    #t{position:fixed;top:0;right:0}
    #a{margin-top:0.5em;display:inline-block}
    a{white-space:nowrap;border:1px solid #ccc;border-radius:4px;padding:3px;margin:3px;font-family:screen1;text-decoration: none;color:#ccc;line-height:2em;}
    .v{display:inline-block;width:0.5em;height:0.5em;margin-right:0.2em;border-radius:50%;background-color:#555;}
    .v.y{background-color:#008000}
  </style>
  </head>
  <body>
    <div id='c'>
      <div class='i'><img id='s' src='${BASE_URL}/cat-image/${catId}'></div>

      ${accessoryList(catId, accessories)}

      <script>(function(){
      var $t=document.getElementById('t'),$a=document.getElementById('a');$p=document.getElementById('p'),$s=document.getElementById('s'),$l=document.getElementById('l'),x=false;y=false;w=100;
      var $b=document.getElementById('b');
      if ($b != null) {
        $b.addEventListener('click', function(){
          if(y){
            $b.innerHTML = 'Owned Accessories ${chevronClosed}';$l.style='display:none;';y=false;
          }else{
            $b.innerHTML = 'Owned Accessories ${chevronOpen}';$l.style='';y=true;
          }
        });
      }
      $t.addEventListener('click',function(){
        if(x){
          $a.style=$p.style='display:none;';$s.style='';$t.innerText='Show Accessories';x=false;
        }else{
          $a.style=$p.style='';$s.style='display:none';$t.innerText='Hide Accessories';x=true
        }})
      })()</script>
    </div>
  </body></html>`;
};
