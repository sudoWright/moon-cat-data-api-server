FROM node:18
RUN groupmod -g 25600 node && usermod -u 25600 -g 25600 node
WORKDIR /app
RUN chown node:node /app

# Install dependencies separately, such that the image layer can be cached
USER node
COPY --chown=node:node package.json ./
RUN npm install

# Copy in the rest of the source code
COPY --chown=node:node . /app
EXPOSE 5000
CMD ["node", "server.js"]